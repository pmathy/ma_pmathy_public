#!/usr/bin/env python3

import subprocess,os,sys,inspect

#add parent directory to sys path to import package
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

from dryPackage import containerInfo

#get container id with function from DRY package
container_id = containerInfo.getContainerID('djangoapp_app1')

helpString = """
-help                   Help Menu
-create [appname]       Create new Django App with name [appname]
-shell                  Run Python Interactive Shell on Django Container
-createsu               Create Django Admin Superuser
-mkmig                  Make Django Database Migrations
-migdb                  Migrate Django Database
-migstat                Migrate Static content for Webserver
-restart                Restart Django App server
"""

### Auxiliary Functions ###

def createApp(contId, appName):
    #concatenate container ID with command to create Django App
    command = "docker exec " + contId + " python manage.py startapp " + appName
    #execute concatenated command
    subprocess.call(command, shell=True)
    sys.exit()

def runShell(contId):
    #concatenate container ID with command to run Python Interactive Shell
    command = "docker exec -it " + container_id + " python manage.py shell"
    #execute concatenated command
    subprocess.call(command, shell=True)
    sys.exit()

def createSuperuser(contId):
    #concatenate container ID with command to create Django Admin superuser
    command = "docker exec -it " + container_id + " python manage.py createsuperuser"
    #execute concatenated command
    subprocess.call(command, shell=True)
    sys.exit()

def makeMigrations(contId):
    #concatenate container ID with command to create Python database migration
    command = "docker exec " + container_id + " python manage.py makemigrations"
    #execute concatenated command
    subprocess.call(command, shell=True)
    sys.exit()

def migrateDb(contId):
    #concatenate container ID with command to run Python database migration
    command = "docker exec " + container_id + " python manage.py migrate"
    #execute concatenated command
    subprocess.call(command, shell=True)
    sys.exit()

def migrateStatic(contId):
    #concatenate container ID with command to copy static files to shared Volume for nginx
    command = "docker exec " + container_id + " python manage.py collectstatic --no-input --clear"
    #execute concatenated command
    subprocess.call(command, shell=True)
    sys.exit()

def restartApp(contId):
    #concatenate container ID with command to restart Django App server
    command = "docker restart " + container_id
    #execute concatenated command
    subprocess.call(command, shell=True)
    sys.exit()

### Switch-Case Statement ###

if len(sys.argv) < 2:
    print("You need to specify a parameter for execution of the corresponding script action.")
    print("The following options exist:")
    print(helpString)

elif sys.argv[1] == '-help' and len(sys.argv) == 2:
    print("You can use the script with the following options:")
    print(helpString)

elif sys.argv[1] == '-create' and len(sys.argv) == 3:
    createApp(container_id, sys.argv[2])

elif sys.argv[1] == '-shell' and len(sys.argv) == 2:
    runShell(container_id)

elif sys.argv[1] == '-createsu' and len(sys.argv) == 2:
    createSuperuser(container_id)

elif sys.argv[1] == '-mkmig' and len(sys.argv) == 2:
    makeMigrations(container_id)

elif sys.argv[1] == '-migdb' and len(sys.argv) == 2:
    migrateDb(container_id)

elif sys.argv[1] == '-migstat' and len(sys.argv) == 2:
    migrateStatic(container_id)

elif sys.argv[1] == '-restart' and len(sys.argv) == 2:
    restartApp(container_id)

else:
    print("The syntax of your command was incorrect. Please refer to the following options:")
    print(helpString)
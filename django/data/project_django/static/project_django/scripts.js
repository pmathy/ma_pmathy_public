function overlay() {
	el = document.getElementById("overlay");
	el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
}
function overlay2() {
	el = document.getElementById("overlay2");
	el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
}
function overlay3() {
	el = document.getElementById("overlay3");
	el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
}

function createMap(drive_locs, pass_locs, dest_locs, routes, arrivalTime) {
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 11,
		center: new google.maps.LatLng(49.435583, 7.836133),
		mapTypeId: google.maps.MapTypeId.ROADMAP
	});

	directionsService = new google.maps.DirectionsService;
    directionsDisplay = new google.maps.DirectionsRenderer({
      map: map
    });
  
	var infowindow = new google.maps.InfoWindow();
  
	var marker, i;
  
	for (i = 0; i < drive_locs.length; i++) {  
		marker = new google.maps.Marker({
		  	position: new google.maps.LatLng(drive_locs[i][1], drive_locs[i][2]),
			map: map,
			icon: {
				url: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png"
			}
		});
  
		google.maps.event.addListener(marker, 'click', (function(marker, i) {
		  	return function() {
				infowindow.setContent(drive_locs[i][0]);
				infowindow.open(map, marker);
		  	}
		})(marker, i));
	};

	for (i = 0; i < pass_locs.length; i++) {  
		marker = new google.maps.Marker({
		  	position: new google.maps.LatLng(pass_locs[i][1], pass_locs[i][2]),
		  	map: map,
			icon: {
			  url: "http://maps.google.com/mapfiles/ms/icons/red-dot.png"
			}
		});
  
		google.maps.event.addListener(marker, 'click', (function(marker, i) {
		  	return function() {
				infowindow.setContent(pass_locs[i][0]);
				infowindow.open(map, marker);
		  	}
		})(marker, i));
	};

	marker = new google.maps.Marker({
		position: new google.maps.LatLng(dest_locs[0][1], dest_locs[0][2]),
		map: map,
		icon: {
			url: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png"
		}
	});
	google.maps.event.addListener(marker, 'click', (function(marker, i) {
		return function() {
		  infowindow.setContent(dest_locs[0][0]);
		  infowindow.open(map, marker);
		}
	  })(marker, i));
	  
	if (routes.length > 0) {
		calculateAndDisplayRoute(directionsService, directionsDisplay, routes, arrivalTime)
	};
}

function calculateAndDisplayRoute(directionsService, directionsDisplay, routes, arrivalTime) {
	for (i = 0; i < routes.length; i++) {
		var pointA = new google.maps.LatLng(routes[i][0][1], routes[i][0][2]),
			pointB = new google.maps.LatLng(routes[i][1][1], routes[i][1][2]);

		const waypts = [];
		for (j = 2; j < routes[i].length; j++) {
			waypts.push({
				location: new google.maps.LatLng(routes[i][j][1], routes[i][j][2]),
				stopover: false,
			});
		}	
		
		directionsService.route({
			origin: pointA,
			destination: pointB,
			waypoints: waypts,
			optimizeWaypoints: true,
			//arrival_time: arrivalTime,
			travelMode: google.maps.TravelMode.DRIVING
		}, function(response, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			directionsDisplay.setDirections(response);
		} else {
			window.alert('Directions request failed due to ' + status);
		}
		});
	};
}
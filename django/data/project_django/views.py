from django.shortcuts import render, redirect
from django.urls import reverse
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout as signout
from django.contrib import messages

def home(request):
    if request.user.is_anonymous:    
        title = 'Home - Fahrerplanung'
        form = AuthenticationForm()

        dictContext = {'form': form, 'title': title}

        if request.method == "POST":
            form = AuthenticationForm(data=request.POST)

            if form.is_valid():
                username = form.cleaned_data.get('username')
                password = form.cleaned_data.get('password')

                user = authenticate(username=username, password=password)

                if user is not None:
                    login(request, user)
                    return redirect(reverse('home'))
                else:
                    messages.error(request, "Invalid username or password.")
                    return render(request, 'project_django/home.html', dictContext)
            else:
                messages.error(request, "Invalid username or password.")
                return render(request, 'project_django/home.html', dictContext)
        else:
            return render(request, 'project_django/home.html', dictContext)
    elif not request.user.profile.commutegroup_set.all():
        return redirect(reverse('usrmgmt:home'))
    else:
        return redirect(reverse('eventplanning:home'))

def logout(request):
    title = 'Home - Fahrerplanung'
    signout(request)
    
    messages.success(request, "You were successfully logged out.")
    return redirect(reverse('home'))
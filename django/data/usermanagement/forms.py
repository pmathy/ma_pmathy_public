from django.apps import apps
from django import forms

from .models import CommuteGroup, Address, Profile
from utils.utils import verifyAddress as verifyAddress

class CommuteGroupCreationForm(forms.ModelForm):
    class Meta:
        model = CommuteGroup
        fields = ['name']

class AddressCreationForm(forms.ModelForm):
    class Meta:
        model = Address
        fields = ['street', 'postal', 'city']

    def clean(self):
        # Runs clean() method of ModelForm (parent Class) and verifies for 
        # Uniqueness etc.
        cleaned_data = super(AddressCreationForm, self).clean()
        
        if not verifyAddress(cleaned_data):
            raise forms.ValidationError(
                "This Address could not be found, please try again with a valid address."
            )

class JoinGroupForm(forms.Form):
    groupCodeField = forms.CharField(min_length=8, max_length=8, required=True,
    label='Join Code', help_text="Please enter the Join Code of the group you want to join.")

    def clean(self):
        # Runs clean() method of ModelForm (parent Class) and verifies for 
        # Uniqueness etc.
        cleaned_data = super(JoinGroupForm, self).clean()
        code=cleaned_data.get('groupCodeField')
        group = CommuteGroup.objects.filter(joinCode=code)

        if not group:
            raise forms.ValidationError(
                "This group code does not exist. Please check for errors."
            )

class ProfileEditForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProfileEditForm, self).__init__(*args, **kwargs)
        self.fields['email'].disabled=True
    class Meta:
        model = apps.get_model('authentication', 'CustomUser')
        fields = ['email','last_name','first_name']

class ChangePassForm(forms.Form):
    # Adds password and pw-confirm form fields as 'hidden' input
    oldPassword=forms.CharField(widget=forms.PasswordInput())
    newPassword=forms.CharField(widget=forms.PasswordInput())
    confirmNewPassword=forms.CharField(widget=forms.PasswordInput())

    def clean(self):
        cleaned_data = super(ChangePassForm, self).clean()
        newPassword = cleaned_data.get("newPassword")
        confirmNewPassword = cleaned_data.get("confirmNewPassword")

        if newPassword != confirmNewPassword:
            raise forms.ValidationError(
                "Password and confirmed password do not match."
            )

class MemberPermissionsForm(forms.Form):
    member = forms.CharField(required=False)
    adminPerm = forms.BooleanField()
    accountantPerm = forms.BooleanField()
    plannerPerm = forms.BooleanField()

    def __init__(self, *args, **kwargs):
        super(MemberPermissionsForm, self).__init__(*args, **kwargs)
        self.fields['member'].disabled=True
    
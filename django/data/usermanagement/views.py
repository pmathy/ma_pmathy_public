from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Permission
from django.contrib.auth import authenticate, login
from django.utils.crypto import get_random_string
from django.forms import formset_factory
from django.contrib import messages
from django.apps import apps
from string import ascii_uppercase, digits

from .forms import CommuteGroupCreationForm, AddressCreationForm, JoinGroupForm, ProfileEditForm, ChangePassForm, MemberPermissionsForm
from .utils import createPermissions
from .models import Address, CommuteGroup, Profile

# Create your views here.

@login_required
def home(request):
    title = 'User Management - Home'
    commGroups = request.user.profile.commutegroup_set.all()

    commgroupForm = CommuteGroupCreationForm()
    joinGroupForm = JoinGroupForm()

    dictContext = {'commgroupForm': commgroupForm, 'joinGroupForm': joinGroupForm,
        'title': title, 'commGroups': commGroups}

    if request.method == "POST" and 'newgroup' in request.POST:
        commgroupForm = CommuteGroupCreationForm(request.POST)

        if commgroupForm.is_valid():
            accName = 'acc_' + commgroupForm.cleaned_data['name']
            commgroupInstance = commgroupForm.save(commit=False)
            commgroupInstance.joinCode = get_random_string(8, ascii_uppercase + digits)
            commgroupInstance.save()

            commgroupInstance.member.add(request.user.profile)
            commgroupInstance.save()

            accName = 'Account_' + commgroupInstance.joinCode
            groupAccount = apps.get_model('accounting', 'Account')(name=accName, group=commgroupInstance)
            groupAccount.save()

            permissions = createPermissions(commgroupInstance.joinCode)
            

            for perm in permissions:
                request.user.user_permissions.add(permissions[perm])
                
            return redirect(reverse('usrmgmt:home'))
        else:
            commgroupForm = CommuteGroupCreationForm()
            return render(request, 'usermanagement/home.html', dictContext)

    elif request.method == "POST" and 'joingroup' in request.POST:
        joinGroupForm = JoinGroupForm(request.POST)

        if joinGroupForm.is_valid():
            code = joinGroupForm.cleaned_data['groupCodeField']
            group = CommuteGroup.objects.get(joinCode=code)

            group.member.add(request.user.profile)

            messages.success(request, "You have joined the group!")
            return redirect(reverse('usrmgmt:home'))
        else:
            messages.error(request, "Invalid")
            joinGroupForm = JoinGroupForm()
            return render(request, 'usermanagement/home.html', dictContext)
 
    else:

        return render(request, 'usermanagement/home.html', dictContext)

@login_required
def profile(request):
    title = 'User Management - Your Profile'
    
    addressList = []
    for address in request.user.profile.address_set.all():
        addressList.append(address)

    addressForm = AddressCreationForm()
    userForm = ProfileEditForm(instance=request.user)

    dictContext = {'addressForm': addressForm, 'userForm': userForm, 'title': title, 'addressList': addressList}

    if request.method == "POST" and 'newaddress' in request.POST:
        addressForm = AddressCreationForm(request.POST)

        if addressForm.is_valid():
            addressInstance = addressForm.save(commit=False)
            addressInstance.profile = request.user.profile
            addressInstance.save()
            return redirect(reverse('usrmgmt:profile'))
        else:
            messages.error(request, "This Address could not be found, please try again with a valid address.")
            addressForm = AddressCreationForm()
            return render(request, 'usermanagement/profile.html', dictContext)
    
    elif request.method == "POST" and 'changeduser' in request.POST:
        userForm = ProfileEditForm(instance=request.user, data=request.POST)

        if userForm.is_valid():
            userInstance = userForm.save()
            messages.success(request, "User Data was successfully modified!")
            return redirect(reverse('usrmgmt:profile'))
        else:
            messages.error(request, "User Modification failed, please check the data you entered.")
            userForm = ProfileEditForm(instance=request.user)
            return render(request, 'usermanagement/profile.html', dictContext)

    else: 
        return render(request, 'usermanagement/profile.html', dictContext)

@login_required
def deleteAddress(request, id):
    title = 'User Management - Delete Address'
    address = get_object_or_404(Address, id=id)

    dictContext = {'title': title,'address': address}

    if request.method == "POST" and request.user.profile == address.profile:
        address.delete()
        messages.success(request, "Address was successfully deleted!")
        return redirect(reverse('usrmgmt:profile'))

    elif request.method == "POST":
        messages.error(request, "You do not have permission to delete this address!")
        return redirect(reverse('usrmgmt:profile'))

    elif not request.user.profile == address.profile:
        messages.error(request, "You do not have permission to delete this address!")
        return redirect(reverse('usrmgmt:profile'))

    else:
        return render(request, 'usermanagement/deladdr.html', dictContext)

@login_required
def deleteGroup(request, id):
    title = 'User Management - Delete Group'
    group = get_object_or_404(CommuteGroup, id=id)
    adminPermName = 'admin_' + group.joinCode
    userPermissions = Permission.objects.filter(user=request.user)
    groupPermissions = Permission.objects.filter(name__icontains=group.joinCode)
    adminPerm = Permission.objects.get(name=adminPermName)

    dictContext = {'title': title,'group': group}

    if request.method == "POST" and adminPerm in userPermissions:
        group.delete()
        for perm in groupPermissions:
            perm.delete()

        messages.success(request, "Group was successfully deleted!")
        return redirect(reverse('usrmgmt:home'))

    elif request.method == "POST":
        messages.error(request, "You do not have permission to delete this group!")
        return redirect(reverse('usrmgmt:home'))

    elif adminPerm not in userPermissions:
        messages.error(request, "You do not have permission to delete this group!")
        return redirect(reverse('usrmgmt:home'))

    else:
        return render(request, 'usermanagement/delgrp.html', dictContext)

@login_required
def changePassword(request):
    title = 'User Management - Change Password'
    changePassForm = ChangePassForm()

    dictContext = {'title': title, 'changePassForm': changePassForm}

    if request.method == "POST":
        changePassForm = ChangePassForm(request.POST)

        if changePassForm.is_valid():
            username = request.user.email
            oldPassword = changePassForm.cleaned_data.get('oldPassword')
            newPassword = changePassForm.cleaned_data.get('newPassword')
            
            user = authenticate(username=username, password=oldPassword)

            if user:
                user.set_password(newPassword)
                user.save()
                login(request, user)
                messages.success(request, "Password was successfully changed!")
                return redirect(reverse('usrmgmt:profile'))

            else:
                messages.error(request, "Wrong password! Please provide correct authentication to change password.")
                return render(request, 'usermanagement/changepass.html', dictContext)
        else:
            changePassForm = ChangePassForm()
            messages.error(request, "Please make sure the new password and confirmed password match!")
            return render(request, 'usermanagement/changepass.html', dictContext)
    else:
        return render(request, 'usermanagement/changepass.html', dictContext)

# IMPORTANT!!! Still missing checks for permission on modifying the object.

@login_required
def groupAdmin(request, id):
    title = 'User Management - Administrate Group'

    group = CommuteGroup.objects.get(id=id)
    members = Profile.objects.filter(commutegroup=group).exclude(user=request.user)

    adminPermName = 'admin_' + group.joinCode
    accountantPermName = 'accountant_' + group.joinCode
    plannerPermName = 'planner_' + group.joinCode
    adminPerm = Permission.objects.get(name=adminPermName)
    accountantPerm = Permission.objects.get(name=accountantPermName)
    plannerPerm = Permission.objects.get(name=plannerPermName)

    initialData = []
    for member in members:
        memberPerms = Permission.objects.filter(name__icontains=group.joinCode, user=member.user)
        dataset = {}
        dataset['member'] = member.user.email
        dataset['adminPerm'] = False
        dataset['accountantPerm'] = False
        dataset['plannerPerm'] = False

        if adminPerm in memberPerms:
            dataset['adminPerm'] = True
        if accountantPerm in memberPerms:
            dataset['accountantPerm'] = True
        if plannerPerm in memberPerms:
            dataset['plannerPerm'] = True

        initialData.append(dataset)

    PermFormSet = formset_factory(MemberPermissionsForm, extra=0)
    formset = PermFormSet(initial=initialData)

    dictContext = {'title': title, 'group': group, 'formset': formset, 'init': initialData}

    if request.method == "POST":
        formset = PermFormSet(initial=initialData, data=request.POST)

        if formset.is_valid():
            for form in formset:
                user = apps.get_model('authentication', 'CustomUser').objects.get(email=form.cleaned_data['member'])

                if form.cleaned_data['adminPerm']:
                    user.user_permissions.add(adminPerm)
                else:
                    user.user_permissions.remove(adminPerm)
                if form.cleaned_data['accountantPerm']:
                    user.user_permissions.add(accountantPerm)
                else:
                    user.user_permissions.remove(accountantPerm)
                if form.cleaned_data['plannerPerm']:
                    user.user_permissions.add(plannerPerm)
                else:
                    user.user_permissions.remove(plannerPerm)
                user.save()
            
            return render(request, 'usermanagement/admgrp.html', dictContext)

        else:
            formset = PermFormSet(initial=initialData)
            messages.error(request, "Please provide valid data!")
            return render(request, 'usermanagement/admgrp.html', dictContext)
    else:
        return render(request, 'usermanagement/admgrp.html', dictContext)

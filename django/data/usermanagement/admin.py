from django.contrib import admin
from .models import Profile, Address, CommuteGroup

# Register your models here.

class ProfileAdmin(admin.ModelAdmin):
    fields = ['user']

class AddressAdmin(admin.ModelAdmin):
    fields = ['profile', 'city', 'street', 'postal']

class CommuteGroupAdmin(admin.ModelAdmin):
    fields = ['name', 'member', 'joinCode']

admin.site.register(Profile, ProfileAdmin)
admin.site.register(Address, AddressAdmin)
admin.site.register(CommuteGroup, CommuteGroupAdmin)
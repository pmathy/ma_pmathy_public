from django.urls import path, re_path

from . import views

app_name = 'usrmgmt'
urlpatterns = [
    path('home', views.home, name='home'),
    path('profile', views.profile, name='profile'),
    path('changepass', views.changePassword, name='changepass'),
    re_path(r'^del/addr/(?P<id>.+)$', views.deleteAddress, name='deladdr'),
    re_path(r'^del/grp/(?P<id>.+)$', views.deleteGroup, name='delgrp'),
    re_path(r'^adm/grp/(?P<id>.+)$', views.groupAdmin, name='admgrp'),
]

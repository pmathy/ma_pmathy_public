# Generated by Django 3.0.1 on 2020-08-30 11:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('usermanagement', '0003_auto_20200830_1027'),
    ]

    operations = [
        migrations.AlterField(
            model_name='commutegroup',
            name='member',
            field=models.ManyToManyField(blank=True, to='usermanagement.Profile'),
        ),
    ]

from django.db import models

# Create your models here.

class Profile(models.Model):
    user = models.OneToOneField("authentication.CustomUser", on_delete=models.CASCADE, primary_key=True)
    
    timestamp_lastupdated = models.DateTimeField(auto_now=True)
    timestamp_added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user.email

class Address(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)

    city = models.CharField(max_length=30)
    street = models.CharField(max_length=30)
    postal = models.PositiveIntegerField()
    
    timestamp_lastupdated = models.DateTimeField(auto_now=True)
    timestamp_added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "%s, %s %s" % (self.street, self.postal, self.city)

class CommuteGroup(models.Model):
    name = models.CharField(max_length=30)
    member = models.ManyToManyField(Profile, blank=True)
    joinCode = models.CharField(max_length=10, default='None')
    
    timestamp_lastupdated = models.DateTimeField(auto_now=True)
    timestamp_added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import Permission

# How to find these permissions later: Permission.objects.get(name=admin_groupcode)
def createPermissions(groupcode):

    groupContentType = ContentType.objects.create(app_label=groupcode, model='none')
    
    adminName = 'admin_'+groupcode
    accName = 'accountant_'+groupcode
    planName = 'planner_'+groupcode

    adminPermission = Permission.objects.create(name=adminName,
        content_type=groupContentType, codename=adminName)
    accountantPermission = Permission.objects.create(name=accName,
        content_type=groupContentType, codename=accName)
    plannerPermission = Permission.objects.create(name=planName,
        content_type=groupContentType, codename=planName)

    permissions = {'admin': adminPermission, 'accountant': accountantPermission,
        'planner': plannerPermission}
    return permissions
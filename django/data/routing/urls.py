from django.urls import path, re_path

from . import views

app_name = 'routing'
urlpatterns = [
    re_path(r'^delete/(?P<eventId>.+)/(?P<vehicleId>.+)/(?P<passengerId>.+)$', views.deletePassenger, name='delpass'),
    re_path(r'^delete/(?P<eventId>.+)/(?P<vehicleId>.+)$', views.deleteVehicle, name='delcar'),
    re_path(r'^create/(?P<eventId>.+)$', views.createRoute, name='createroute'),
    re_path(r'^confirm/(?P<eventId>.+)$', views.confirmRoute, name='confirmroute'),
]

from django.contrib import admin
from .models import Route, Vehicle, Passenger

# Register your models here.

class RouteAdmin(admin.ModelAdmin):
    fields = ['event']

class VehicleAdmin(admin.ModelAdmin):
    fields = ['route', 'driver']

class PassengerAdmin(admin.ModelAdmin):
    fields = ['vehicle', 'passenger']

admin.site.register(Route, RouteAdmin)
admin.site.register(Vehicle, VehicleAdmin)
admin.site.register(Passenger, PassengerAdmin)
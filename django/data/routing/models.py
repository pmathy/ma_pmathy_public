from django.db import models

# Create your models here.

class Route(models.Model):
    event = models.OneToOneField("eventplanning.Event", on_delete=models.CASCADE, primary_key=True)
    isCompleted = models.BooleanField(default=False)
    
    timestamp_lastupdated = models.DateTimeField(auto_now=True)
    timestamp_added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "Route to %s" % (self.event.name)

class Vehicle(models.Model):
    route = models.ForeignKey(Route, on_delete=models.CASCADE)
    driver = models.OneToOneField("eventplanning.Attendee", on_delete=models.CASCADE)

    def __str__(self):
        return "Vehicle for %s" % (self.route)

class Passenger(models.Model):
    vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE)
    passenger = models.OneToOneField("eventplanning.Attendee", on_delete=models.CASCADE, null=True)

    def __str__(self):
        return "%s" % (self.passenger)
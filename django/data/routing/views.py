from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, reverse
from django.contrib import messages
from django.apps import apps
from django.forms import modelformset_factory
from datetime import datetime

from .models import Route, Vehicle, Passenger
from .forms import VehicleCreationForm, createPassengerForm
from utils.utils import createMapsApiSrc as createApiString
from utils.utils import createLocations as createLocations
from utils.utils import createRoutes as createRoutes

# IMPORTANT!!! Still missing checks for permission on modifying the object.

@login_required
def createRoute(request, eventId):

    title = 'Route Creation'
    showNew = True
    apiString = createApiString("&callback=initMap")

    event = apps.get_model('eventplanning', 'Event').objects.get(id=eventId)
    route = event.route
    vehicles = route.vehicle_set.all()

    drivers = apps.get_model('eventplanning', 'Attendee').objects.filter(event=event, isDriver=True, isTaken=False)
    passengers = apps.get_model('eventplanning', 'Attendee').objects.filter(event=event, isTaken=False)

    initialCarDict = {'route': route}
    vehicleForm = VehicleCreationForm(initial=initialCarDict, drivers=drivers)
    formsetInstance = ()

    if vehicles: 
        for car in vehicles:
            if not car.passenger_set.all():
                showNew = False
                # initialize Passenger Form here
                slots = car.driver.slots
                initialPassDict = {'vehicle': car}
                initialPassList = []
                for i in range(slots):
                    initialPassList.append(initialPassDict)

                passengerCreationForm = createPassengerForm(passengers)
                PassengerFormSet = modelformset_factory(Passenger, form=passengerCreationForm, extra=slots)
                formsetInstance = PassengerFormSet(initial=initialPassList)

    locations = createLocations(event)
    driver_locs = locations['driver_loc']
    passenger_locs = locations['pass_loc']
    destination_locs = locations['dest_loc']
    if vehicles:
        routes = createRoutes(vehicles)
    else:
        routes = []
    arrivalTime = int((datetime.combine(event.eventDate, event.arrivalTime) - datetime(1970,1,1)).total_seconds())

    canFinish = True
    for attendee in event.attendee_set.all():
        if not attendee.isTaken:
            canFinish = False

    dictContext = {'title': title, 
        'event': event, 
        'vehicles': vehicles, 
        'vehicleForm': vehicleForm, 
        'formset': formsetInstance,
        'showNew': showNew,
        'apiString': apiString,
        'driver_locs': driver_locs,
        'passenger_locs': passenger_locs,
        'destination_locs': destination_locs,
        'routes': routes,
        'arrivalTime': arrivalTime,
        'canFinish': canFinish,
    }

    if request.method == "POST" and 'newvehicle' in request.POST:
        vehicleForm = VehicleCreationForm(initial=initialCarDict, drivers=drivers, data=request.POST)

        if vehicleForm.is_valid():
            driver = vehicleForm.cleaned_data['driver']
            driver.isTaken = True
            driver.save()

            vehicleInstance = vehicleForm.save()

            messages.success(request, str(driver) + " is now a driver and can be equipped with passengers.")
            return redirect(reverse('routing:createroute', kwargs={'eventId': eventId}))
        else:
            vehicleForm = VehicleCreationForm(initial=initialCarDict, drivers=drivers)
            return render(request, 'routing/createroute.html', dictContext)

    elif request.method == "POST" and 'newpassengers' in request.POST:
        formsetInstance = PassengerFormSet(initial=initialPassList, data=request.POST)

        if formsetInstance.is_valid():
            for form in formsetInstance:
                if form.cleaned_data:
                    attendee = form.cleaned_data['passenger']
                    attendee.isTaken = True
                    attendee.save()

            formsetInstance.save()

            messages.success(request, "Passengers were added successfully.")
            return redirect(reverse('routing:createroute', kwargs={'eventId': eventId}))
        else:
            formsetInstance = PassengerFormSet(initial=initialPassList)
            return render(request, 'routing/createroute.html', dictContext)

    else:
        return render(request, 'routing/createroute.html', dictContext)

@login_required
def deleteVehicle(request, eventId, vehicleId):
    vehicle = Vehicle.objects.get(id=vehicleId)
    vehicle.driver.isTaken = False
    vehicle.driver.save()

    for passenger in vehicle.passenger_set.all():
        passenger.passenger.isTaken = False
        passenger.passenger.save()

    vehicle.delete()
    messages.success(request, "Vehicle was successfully deleted.")

    return redirect(reverse('routing:createroute', kwargs={'eventId': eventId}))
    
@login_required    
def deletePassenger(request, eventId, vehicleId, passengerId):
    passenger = Passenger.objects.get(id=passengerId)
    passenger.passenger.isTaken = False
    passenger.passenger.save()
    passenger.delete()
    messages.success(request, "Passenger was successfully deleted.")

    return redirect(reverse('routing:createroute', kwargs={'eventId': eventId}))

@login_required
def confirmRoute(request, eventId):
    event = apps.get_model('eventplanning', 'Event').objects.get(id=eventId)
    route = event.route

    return redirect(reverse('eventplanning:plan'))
from django.forms.widgets import HiddenInput
from django.apps import apps
from django import forms
import re

from .models import Route, Vehicle, Passenger

class VehicleCreationForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        drivers = kwargs.pop('drivers')
        super(VehicleCreationForm, self).__init__(*args, **kwargs)
        self.fields['route'].disabled=True
        self.fields['driver'] = forms.ModelChoiceField(
            queryset=drivers
        )
    class Meta:
        model = Vehicle
        fields = ['route', 'driver']

def createPassengerForm(passengers):
    class PassengerCreationForm(forms.ModelForm):
        def __init__(self, *args, **kwargs):
            super(PassengerCreationForm, self).__init__(*args, **kwargs)
            self.fields['vehicle'].disabled=True
            self.fields['vehicle'].widget = HiddenInput()

            self.fields['passenger'] = forms.ModelChoiceField(
                queryset=passengers
            )
        class Meta:
            model = Passenger
            fields = ['vehicle','passenger']

    return PassengerCreationForm
    
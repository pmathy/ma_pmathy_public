from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.contrib import messages
from django.apps import apps
from datetime import date

from .utils import getEditablesForPlanner as eventForPlanner
from .forms import EventCreationForm, EventEditForm, AttendeeCreationForm, DestinationCreationForm
from .models import Event, Attendee

# Create your views here.

@login_required
def home(request):
    title = 'Events'

    groups = request.user.profile.commutegroup_set.all()
    events = Event.objects.filter(group__in=groups, eventDate__gte=date.today())

    signedUpEvents = []

    for event in events:
        for attendee in request.user.profile.attendee_set.all():
            if attendee in event.attendee_set.all() and not event in signedUpEvents:
                signedUpEvents.append(event)

    dictContext = {'title': title, 'events': events, 'signedUpEvents': signedUpEvents}

    return render(request, 'eventplanning/home.html', dictContext)

@login_required
def plan(request):

    title = 'Eventplanning'
    editableEvents = eventForPlanner(request.user, key='events')
    editableGroups = eventForPlanner(request.user, key='groups')
    eventForm = EventCreationForm(groups=editableGroups)
    destForm = DestinationCreationForm()
    dictContext = {'eventForm': eventForm, 'destForm': destForm, 'title': title, 'events': editableEvents}

    if request.method == "POST" and 'newevent' in request.POST:
        eventForm = EventCreationForm(groups=editableGroups, data=request.POST)
        destForm = DestinationCreationForm(data=request.POST)

        if eventForm.is_valid() and destForm.is_valid():
            eventInstance = eventForm.save()
            destInstance = destForm.save(commit=False)

            destInstance.event = eventInstance
            destInstance.save()

            eventRoute = apps.get_model('routing', 'Route')(event=eventInstance)
            eventRoute.save()

            return redirect(reverse('eventplanning:plan'))
        else:
            messages.error(request, "Please enter valid data for the event and destination!\nRemember that the Arrival Time must be equal or earlier than the Event Time!")
            eventForm = EventCreationForm(groups=editableGroups)
            destForm = DestinationCreationForm()
            return render(request, 'eventplanning/plan.html', dictContext)
    
    else:
        return render(request, 'eventplanning/plan.html', dictContext)

@login_required
def deleteEvent(request, id):
    title = 'User Management - Delete Event'
    editableEvents = eventForPlanner(request.user, key='events')
    event = get_object_or_404(Event, id=id)

    dictContext = {'title': title,'event': event}

    if request.method == "POST" and event in editableEvents:
        event.delete()

        messages.success(request, "Event was successfully deleted!")
        return redirect(reverse('eventplanning:plan'))

    elif request.method == "POST":
        messages.error(request, "You do not have permission to delete this event!")
        return redirect(reverse('eventplanning:plan'))

    elif not event in editableEvents:
        messages.error(request, "You do not have permission to delete this event!")
        return redirect(reverse('eventplanning:plan'))

    else:
        return render(request, 'eventplanning/delevent.html', dictContext)

@login_required
def editEvent(request, id):
    title = 'User Management - Edit Event'
    editableEvents = eventForPlanner(request.user, key='events')
    event = get_object_or_404(Event, id=id)
    groupQuerySet = apps.get_model('usermanagement', 'CommuteGroup').objects.filter(event=event.id)
    eventEditForm = EventEditForm(instance=event, groups=groupQuerySet)

    dictContext = {'title': title,'event': event, 'eventEditForm': eventEditForm}

    if request.method == "POST" and event in editableEvents:
        eventEditForm = EventEditForm(instance=event, data=request.POST, groups=groupQuerySet)
        
        if eventEditForm.is_valid():
            editedEvent = eventEditForm.save()
            messages.success(request, "Event was successfully edited!")
            return redirect(reverse('eventplanning:plan'))
        else:
            messages.error(request, "Please enter valid data for the event!\nRemember that the Arrival Time must be equal or earlier than the Event Time!")
            eventEditForm = EventEditForm(instance=event, groups=groupQuerySet)
            return render(request, 'eventplanning/editevent.html', dictContext)

    elif request.method == "POST":
        messages.error(request, "You do not have permission to edit this event!")
        return redirect(reverse('eventplanning:plan'))

    elif not event in editableEvents:
        messages.error(request, "You do not have permission to edit this event!")
        return redirect(reverse('eventplanning:plan'))

    else:
        return render(request, 'eventplanning/editevent.html', dictContext)

@login_required
def attendEvent(request, id):
    title = 'User Management - Attend Event'
    groups = request.user.profile.commutegroup_set.all()
    userEvents = Event.objects.filter(group__in=groups, eventDate__gte=date.today())
    thisEvent = get_object_or_404(Event, id=id)
    initialDict = {'event': thisEvent}
    addrs = request.user.profile.address_set.all()

    attendeeCreationForm = AttendeeCreationForm(initial=initialDict, addrs=addrs)

    dictContext = {'title': title,'event': thisEvent, 'attendeeCreationForm': attendeeCreationForm}

    if request.method == "POST" and thisEvent in userEvents:
        
        attendeeCreationForm = AttendeeCreationForm(initial=initialDict, addrs=addrs, data=request.POST)
        
        if attendeeCreationForm.is_valid():
            attendedEvent = attendeeCreationForm.save(commit=False)
            attendedEvent.user = request.user.profile
            attendedEvent.save()

            messages.success(request, "You are now attending event " + thisEvent.name + "!")
            return redirect(reverse('eventplanning:home'))
        else:
            attendeeCreationForm = AttendeeCreationForm(initial=initialDict, addrs=addrs)
            return render(request, 'eventplanning/attevent.html', dictContext)

    elif request.method == "POST":
        messages.error(request, "You do not have permission to attend this event!")
        return redirect(reverse('eventplanning:home'))

    elif not thisEvent in userEvents:
        messages.error(request, "You do not have permission to attend this event!")
        return redirect(reverse('eventplanning:home'))

    else:
        return render(request, 'eventplanning/attevent.html', dictContext)

@login_required
def leaveEvent(request, id):
    title = 'User Management - Leave Event'
    event = get_object_or_404(Event, id=id)
    attendee = Attendee.objects.get(event=event, user=request.user.profile)

    if attendee:
        messages.success(request, "You are no longer attending event " + attendee.event.name + "!")
        attendee.delete()
        return redirect(reverse('eventplanning:home'))

    else:
        messages.error(request, "You do not have permission to modify attendance here!")
        return redirect(reverse('eventplanning:home'))

@login_required
def editAttendance(request, id):
    title = 'User Management - Edit Attendance'

    event = get_object_or_404(Event, id=id)
    attendee = Attendee.objects.get(event=event, user=request.user.profile)
    addrs = request.user.profile.address_set.all()

    attendeeCreationForm = AttendeeCreationForm(instance=attendee, addrs=addrs)
    dictContext = {'title': title,'event': event, 'attendeeCreationForm': attendeeCreationForm}

    if request.method == "POST" and attendee:
        attendeeCreationForm = AttendeeCreationForm(instance=attendee, addrs=addrs, data=request.POST)
        
        if attendeeCreationForm.is_valid():
            modifiedAttendee = attendeeCreationForm.save()
            messages.success(request, "Attendance successfully modified!")
            return redirect(reverse('eventplanning:home'))
        else:
            attendeeCreationForm = AttendeeCreationForm(instance=attendee, addrs=addrs)
            return render(request, 'eventplanning/attedit.html', dictContext)

    elif request.method == "POST":
        messages.error(request, "You do not have permission to edit this object!")
        return redirect(reverse('eventplanning:home'))

    elif attendee:
        attendeeCreationForm = AttendeeCreationForm(instance=attendee, addrs=addrs)
        return render(request, 'eventplanning/attedit.html', dictContext)

    else:
        messages.error(request, "You do not attend this event.")
        return redirect(reverse('eventplanning:home'))
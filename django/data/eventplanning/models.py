from django.db import models

# Create your models here.

class Event(models.Model):
    name = models.CharField(max_length=30)
    group = models.ForeignKey("usermanagement.CommuteGroup", on_delete=models.CASCADE, null=True)
    eventDate = models.DateField(null=True)
    eventTime = models.TimeField(null=True)
    arrivalTime = models.TimeField(null=True)
    accountedFor = models.BooleanField(default=False)
    
    timestamp_lastupdated = models.DateTimeField(auto_now=True)
    timestamp_added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

class Attendee(models.Model):
    user = models.ForeignKey("usermanagement.Profile", on_delete=models.PROTECT)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    address = models.ForeignKey("usermanagement.Address", on_delete=models.PROTECT, null=True)
    isDriver = models.BooleanField(default=False)
    slots = models.PositiveIntegerField(default=0)
    isTaken = models.BooleanField(default=False)
    
    timestamp_lastupdated = models.DateTimeField(auto_now=True)
    timestamp_added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "%s" % self.user

class Destination(models.Model):
    event = models.OneToOneField(Event, on_delete=models.CASCADE, primary_key=True)

    city = models.CharField(max_length=30)
    street = models.CharField(max_length=30)
    postal = models.PositiveIntegerField()
    
    timestamp_lastupdated = models.DateTimeField(auto_now=True)
    timestamp_added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "%s (%s, %s)" % (self.event.name, self.street, self.city)
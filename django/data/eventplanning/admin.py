from django.contrib import admin
from .models import Event, Attendee, Destination

# Register your models here.

class EventAdmin(admin.ModelAdmin):
    fields = ['name', 'group', 'eventDate', 'eventTime', 'arrivalTime']

class AttendeeAdmin(admin.ModelAdmin):
    fields = ['user', 'event', 'isDriver', 'isTaken']

class DestinationAdmin(admin.ModelAdmin):
    fields = ['event', 'city', 'street', 'postal']

admin.site.register(Event, EventAdmin)
admin.site.register(Attendee, AttendeeAdmin)
admin.site.register(Destination, DestinationAdmin)
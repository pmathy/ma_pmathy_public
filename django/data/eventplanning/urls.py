from django.urls import path, re_path

from . import views

app_name = 'eventplanning'
urlpatterns = [
    path('home', views.home, name='home'),
    path('plan', views.plan, name='plan'),
    re_path(r'^del/event/(?P<id>.+)$', views.deleteEvent, name='delevent'),
    re_path(r'^edit/event/(?P<id>.+)$', views.editEvent, name='editevent'),
    re_path(r'^att/event/(?P<id>.+)$', views.attendEvent, name='attevent'),
    re_path(r'^attedit/event/(?P<id>.+)$', views.editAttendance, name='attedit'),
    re_path(r'^leave/event/(?P<id>.+)$', views.leaveEvent, name='leaveevent'),
]

from django.contrib.auth.models import Permission
from django.db.models import Q
from django.apps import apps

from .models import Event


def getEditablesForPlanner(user, key='events'):
    groupCodes = []

    planPermissions = Permission.objects.filter(Q(name__icontains='admin') | Q(name__icontains='planner'), user=user)

    for perm in planPermissions:
        splits = perm.name.split('_')
        code = splits[1]

        if not code in groupCodes:
            groupCodes.append(code)
    
    groups = apps.get_model('usermanagement', 'CommuteGroup').objects.filter(joinCode__in=groupCodes)

    events = Event.objects.filter(group__in=groups)

    if key == 'events':
        return events
    elif key == 'groups':
        return groups


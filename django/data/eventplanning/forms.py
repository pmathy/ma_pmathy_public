from django.apps import apps
from django import forms

from .models import Event, Attendee, Destination
from utils.utils import verifyAddress as verifyAddress

class EventCreationForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        groups = kwargs.pop('groups')
        super(EventCreationForm, self).__init__(*args, **kwargs)
        self.fields['group'] = forms.ModelChoiceField(
            queryset=groups
        )
    class Meta:
        model = Event
        fields = ['group', 'name', 'eventDate', 'eventTime', 'arrivalTime']
        widgets = {
        'eventDate': forms.DateInput(format=('%Y-%m-%d'), attrs={'class':'form-control', 'placeholder':'Select Event date', 'type':'date'}),
        'eventTime': forms.DateInput(format=('%H:%M'), attrs={'class':'form-control', 'placeholder':'Select Event time', 'type':'time'}),
        'arrivalTime': forms.DateInput(format=('%H:%M'), attrs={'class':'form-control', 'placeholder':'Select Arrival time', 'type':'time'}),
        }

    def clean(self):
        # Runs clean() method of ModelForm (parent Class) and verifies for 
        # Uniqueness etc.
        cleaned_data = super(EventCreationForm, self).clean()
        eventTime=cleaned_data.get('eventTime')
        arrivalTime=cleaned_data.get('arrivalTime')

        if arrivalTime > eventTime:
            raise forms.ValidationError(
                "The time of arrival must be equal or earlier than the event start time."
            )

class EventEditForm(EventCreationForm):
    def __init__(self, *args, **kwargs):
        super(EventEditForm, self).__init__(*args, **kwargs)
        self.fields['group'].disabled=True

class AttendeeCreationForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        addrs = kwargs.pop('addrs')
        super(AttendeeCreationForm, self).__init__(*args, **kwargs)
        self.fields['event'].disabled=True
        self.fields['address'] = forms.ModelChoiceField(
            queryset=addrs
        )

    class Meta:
        model = Attendee
        fields = ['event', 'isDriver', 'slots', 'address']

    def clean(self):
        # Runs clean() method of ModelForm (parent Class) and verifies for 
        # Uniqueness etc.
        cleaned_data = super(AttendeeCreationForm, self).clean()
        
        if cleaned_data['isDriver'] and not cleaned_data['slots'] > 0:
            raise forms.ValidationError(
                "If you are a driver, you must have more than 0 slots in your car."
            )

class DestinationCreationForm(forms.ModelForm):
    class Meta:
        model = Destination
        fields = ['city', 'street', 'postal']

    def clean(self):
        # Runs clean() method of ModelForm (parent Class) and verifies for 
        # Uniqueness etc.
        cleaned_data = super(DestinationCreationForm, self).clean()
        
        if not verifyAddress(cleaned_data):
            raise forms.ValidationError(
                "This Address could not be found, please try again with a valid address."
            )
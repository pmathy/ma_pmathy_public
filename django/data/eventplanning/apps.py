from django.apps import AppConfig


class EventplanningConfig(AppConfig):
    name = 'eventplanning'

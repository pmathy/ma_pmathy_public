import json, requests

def getAddressData(address):
    baseUrl = "https://maps.googleapis.com/maps/api/geocode/json?address="
    keyPath = "../api_key.json"
    with open(keyPath) as json_file:
        data = json.load(json_file)
    key = "&key=" + data['serverkey']

    if type(address) is dict:
        addressUrl = address['street']+","+str(address['postal'])+","+address['city']
    else:
        addressUrl = address.street+","+str(address.postal)+","+address.city

    url = baseUrl + addressUrl + key

    response = requests.get(url)

    return response

def makeWaypointList(wpList, obj):
    if hasattr(obj, 'address'):
        response = getAddressData(obj.address)
    else:
        response = getAddressData(obj.destination)
    
    content = json.loads(response.text)
    wpList.append(str(obj))
    wpList.append(content['results'][0]['geometry']['location']['lat'])
    wpList.append(content['results'][0]['geometry']['location']['lng'])
    return wpList

def verifyAddress(address):
    response = getAddressData(address)

    if json.loads(response.text)['status'] == "OK":
        return True
    else:
        return False

def createMapsApiSrc(qstring=""):
    baseString = "<script src=\"https://maps.googleapis.com/maps/api/js"
    closeString = "\" type=\"text/javascript\"></script>"

    keyPath = "../api_key.json"
    with open(keyPath) as json_file:
        data = json.load(json_file)
    key = "?key=" + data['browserkey']

    output = baseString + key + qstring + closeString
    return output

def createLocations(event):
    driver_locations = []
    passenger_locations = []
    destination_locations = []
    
    dest = []
    dest = makeWaypointList(dest, event)
    dest.append(1)
    destination_locations.append(dest)

    for index, attendee in enumerate(event.attendee_set.all()):
        location = []
        location = makeWaypointList(location, attendee)
        location.append(index+2)

        if attendee.isDriver == True:
            driver_locations.append(location)
        else:
            passenger_locations.append(location)

    return {"dest_loc": destination_locations, "driver_loc": driver_locations, "pass_loc": passenger_locations}

def createRoutes(vehicles):
    routes = []

    dest = []
    dest = makeWaypointList(dest, vehicles.first().route.event)

    for car in vehicles:
        route = []

        start = []
        start = makeWaypointList(start, car.driver)
        route.append(start)
        route.append(dest)

        for passenger in car.passenger_set.all():
            waypoint = []
            waypoint = makeWaypointList(waypoint, passenger.passenger)
            route.append(waypoint)

        routes.append(route)

    return routes

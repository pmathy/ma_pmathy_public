from django.contrib import admin
from .models import Account, Bill, BillEntry

# Register your models here.

class AccountAdmin(admin.ModelAdmin):
    fields = ['name', 'group']

class BillAdmin(admin.ModelAdmin):
    fields = ['account', 'billdate']

class BillEntryAdmin(admin.ModelAdmin):
    fields = ['event', 'bill']

admin.site.register(Account, AccountAdmin)
admin.site.register(Bill, BillAdmin)
admin.site.register(BillEntry, BillEntryAdmin)
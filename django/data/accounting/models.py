from django.db import models

# Create your models here.

class Account(models.Model):
    name = models.CharField(max_length=30)
    group = models.OneToOneField("usermanagement.CommuteGroup", on_delete=models.CASCADE)
    
    timestamp_lastupdated = models.DateTimeField(auto_now=True)
    timestamp_added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

class Bill(models.Model):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    billdate = models.DateField(null=True)
    
    timestamp_lastupdated = models.DateTimeField(auto_now=True)
    timestamp_added = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ("account", "billdate")

    def __str__(self):
        return "Bill %s, Account %s, %s" % (self.id, self.account.name, self.billdate)
    
    def sendBill(self):
        return false

class BillEntry(models.Model):
    event = models.OneToOneField("eventplanning.Event", on_delete=models.CASCADE)
    bill = models.ForeignKey(Bill, on_delete=models.CASCADE)
    
    timestamp_lastupdated = models.DateTimeField(auto_now=True)
    timestamp_added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.event.name
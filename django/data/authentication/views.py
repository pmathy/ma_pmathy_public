from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone
from django.urls import reverse
from django.apps import apps
from django import forms


import datetime

from .forms import SignupForm
from .models import CustomUser

from .utils import createSaltedHash as salthash
from .utils import sendActivation as actmail

def signup(request):
    title = 'Signup - Fahrerplanung'
    form = SignupForm()

    dictContext = {'form': form, 'title': title}

    if request.method == "POST":
        form = SignupForm(request.POST)

        if form.is_valid():
            userInstance = form.save(commit=False)
            userInstance.set_password(form.cleaned_data.get("password"))
            userInstance.is_active = False
            userInstance.timestamp = timezone.now()
            userInstance.activation_key = salthash(userInstance.email)
            userInstance.actkey_expires = datetime.datetime.strftime(datetime.datetime.now() + datetime.timedelta(days=1), "%Y-%m-%d %H:%M:%S")
            
            userProfile = apps.get_model('usermanagement', 'Profile')(user=userInstance)

            mailSent = actmail(userInstance)

            if mailSent <= 0:
                raise forms.ValidationError("The entered E-Mail Address is not valid, an activation message could not be sent.")
                return render(request, 'authentication/signup.html', dictContext)
            else:
                userInstance.save()
                userProfile.save()
                return redirect(reverse('home'))
        else:
            return render(request, 'authentication/signup.html', dictContext)
 
    else:
  
        return render(request, 'authentication/signup.html', dictContext)

def activate(request, key):
    title = ''
    user = get_object_or_404(CustomUser, activation_key=key)
    dictContext = {'username': user.first_name, 'title': title}

    if user.is_active == False:
        if timezone.now() > user.actkey_expires: # Activation key expired
            title = 'Activation Expired'
            userId = user.id
            dictContext.update({'ID': userId})
            return render(request, 'authentication/actExpired.html', dictContext)

        else: # Activation successful
            title = 'Activation Success'
            user.is_active = True
            user.save()
            return render(request, 'authentication/actSuccess.html', dictContext)
    
    else:
        title = 'Already Active'
        return render(request, 'authentication/activeAlready.html', dictContext)

def newLink(request, user_id):
    user = CustomUser.objects.get(id=user_id)

    if user is not None and not user.is_active:
        user.activation_key = salthash(user.last_name)
        user.actkey_expires = datetime.datetime.strftime(datetime.datetime.now() + datetime.timedelta(days=1), "%Y-%m-%d %H:%M:%S")
        user.save()

        actmail(user)
    return redirect(reverse('home'))
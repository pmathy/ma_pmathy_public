from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.forms import ModelForm
from django import forms

from .models import CustomUser

# Added modified UserCreationForm for Admin Site user creation
class CustomUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm):
        model = CustomUser
        fields = ('email','last_name','first_name',)

class CustomUserChangeForm(UserChangeForm):

    class Meta:
        model = CustomUser
        fields = ('email','last_name','first_name',)

class SignupForm(ModelForm):
    # Adds password and pw-confirm form fields as 'hidden' input
    password=forms.CharField(widget=forms.PasswordInput())
    confirm_password=forms.CharField(widget=forms.PasswordInput())
    class Meta:
        model = CustomUser
        fields = ['email','last_name','first_name','password']

    def clean(self):
        # Runs clean() method of ModelForm (parent Class) and verifies for 
        # Uniqueness etc.
        cleaned_data = super(SignupForm, self).clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")

        if password != confirm_password:
            raise forms.ValidationError(
                "Password and confirm_password do not match."
            )


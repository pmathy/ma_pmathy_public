from django.urls import path, re_path

from . import views

app_name = 'authentication'
urlpatterns = [
    path('signup', views.signup, name='signup'),
    re_path(r'^activate/(?P<key>.+)$', views.activate, name='activation'),
    re_path(r'^newactivation/(?P<user_id>\d+)/$', views.newLink, name='new_activation_link'),
]

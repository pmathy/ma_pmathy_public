'''
Helper functions to outsource specific tasks to clean up code.
'''

import hashlib

from django.utils.crypto import get_random_string
from django.core import mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags

def createSaltedHash(username):
    charset = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
    secret_key = get_random_string(20, charset)
    toHash = (secret_key+username).encode('utf-8')

    saltedHash = hashlib.sha256(toHash).hexdigest()
    return saltedHash


def sendActivation(recipient):
    activationLink = "http://localhost:8080/auth/activate/" + recipient.activation_key
    subject = 'Deine fahrerplanung.de Account Aktivierung'
    html_message = render_to_string('authentication/activateEmail.html',
        {'username': recipient.first_name,
        'link': activationLink})
    plain_message = strip_tags(html_message)
    from_email = 'Aktivierung <do-not-reply@fahrerplanung.de>'
    to = recipient.email

    return mail.send_mail(subject, plain_message, from_email, [to], html_message=html_message, fail_silently=True)
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _

from .managers import CustomUserManager

# Custom User class based on AbstractUser, changed to implement Mail Address as 
# primary attribute
class CustomUser(AbstractUser):
    username = None
    email = models.EmailField(_('email address'), 
        unique=True, 
        error_messages={
        'unique':"A user with this address already exists. Please use a different address."
        }
    )
    # Attributes identical to base class, but neccessary to add as REQUIRED_FIELDS
    last_name = models.CharField(max_length=150, verbose_name='last name')
    first_name = models.CharField(max_length=30, verbose_name='first name')
    activation_key = models.CharField(max_length=64, 
        verbose_name='activation key', 
        default='admin created')
    actkey_expires = models.DateTimeField(null=True)
    timestamp_lastupdated = models.DateTimeField(auto_now=True)
    timestamp_added = models.DateTimeField(auto_now_add=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['last_name', 'first_name']

    # Linked to customized UserManager class
    objects = CustomUserManager()

    def __str__(self):
        return self.email
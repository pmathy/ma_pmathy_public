# Generated by Django 3.0.1 on 2020-06-01 17:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0006_auto_20200531_1918'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='activation_key',
            field=models.CharField(default='admin created', max_length=64, verbose_name='activation key'),
        ),
        migrations.AddField(
            model_name='customuser',
            name='actkey_expires',
            field=models.DateTimeField(null=True),
        ),
    ]
